import 'dart:ui';

import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Container(
          height: MediaQuery.sizeOf(context).height,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset("assets/images/2.jpg", fit: BoxFit.cover),

              Positioned.fill(
                child: Center(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 15.0,
                      sigmaY: 15.0,
                    ),
                    child: Container(
                      height: 200,
                      width: MediaQuery.sizeOf(context).width,
                      // color: Colors.black.withOpacity(0.2),
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Sign Up",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 330,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.rectangle,
                              boxShadow: const [
                                BoxShadow(
                                  blurRadius: 5,
                                  color: Color.fromARGB(119, 0, 0, 0),
                                ),
                              ],
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: TextFormField(
                              decoration: InputDecoration(
                                labelText: "Enter the email",
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              // Container(
              //   child: Column(
              //     children: [

              //       Text(
              //         "Sign Up",
              //         style: TextStyle(
              //             color: Colors.white,
              //             fontSize: 30,
              //             fontWeight: FontWeight.bold),
              //       ),
              //     ],
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
